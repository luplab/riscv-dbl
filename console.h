// SPDX-License-Identifier: GPL-2.0
/*
 * Copyright (c) 2021 Joël Porquet-Lupine
 */
#ifndef _CONSOLE_H
#define _CONSOLE_H

#include <stdarg.h>
#include <stddef.h>

/*
 * Frontend printing function
 */
int printf(const char *fmt, ...);

/*
 * Backend console management
 */
typedef void (*console_putc_t)(const char);
void console_register(console_putc_t driver);

struct console_driver {
    const char *compatible;
    int (*init)(unsigned long node);
};

#define console_driver_init(compatible, init)           \
    __attribute__((__used__))                           \
    __attribute__((__section__(".console_driver")))     \
    static const struct console_driver __console_##init \
    = { compatible, init }

void setup_console(void);

#endif /* _CONSOLE_H */
