// SPDX-License-Identifier: GPL-2.0
/*
 * Copyright (c) 2021 Joël Porquet-Lupine
 */
#include <stdint.h>

#include "console.h"
#include "fdt.h"

static void *lupio_tty_addr;

static void lupio_tty_putc(const char c)
{
    *(volatile uint32_t *)lupio_tty_addr = c;
}

static int lupio_tty_init(unsigned long node)
{
    void *reg;
    unsigned long s;

    /* Get mapping address */
    reg = fdt_get_prop(node, "reg", &s);
    if (!reg)
        return -1;

    fdt_read_addr_size(reg, (uint64_t*)&lupio_tty_addr, NULL, s);

    /* Register our tty device */
    console_register(lupio_tty_putc);
    return 1;
}

console_driver_init("lupio,tty", lupio_tty_init);
