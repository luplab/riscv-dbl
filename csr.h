// SPDX-License-Identifier: GPL-2.0
/*
 * Copyright (c) 2021 Joël Porquet-Lupine
 */
#ifndef _CSR_H
#define _CSR_H

/*
 * User-level CSR
 */

/* Floating point */
#define CSR_FCSR        0x003   /* FP control and status */


/*
 * Supervisor-level CSR
 */

/* Trap setup */
#define CSR_SSTATUS     0x100   /* Status */
#define CSR_SEDELEG     0x102   /* Exception delegation */
#define CSR_SIDELEG     0x103   /* Interrupt delegation */
#define CSR_SIE         0x104   /* Interrupt-enable */
#define CSR_STVEC       0x105   /* Trap handler base address */
#define CSR_SCOUNTEREN  0x106   /* Counter enable */

/* Trap handling */
#define CSR_SSCRATCH    0x140   /* Scratch register */
#define CSR_SEPC        0x141   /* Exception program counter */
#define CSR_SCAUSE      0x142   /* Trap cause */
#define CSR_STVAL       0x143   /* Bad address or instruction */
#define CSR_SIP         0x144   /* Interrupt pending */

/* Memory protection and translation */
#define CSR_SATP        0x180   /* Address translation and protection */


/*
 * Machine-level CSR
 */

/* Machine info */
#define CSR_MVENDORID   0xf11
#define CSR_MARCHID     0xf12
#define CSR_MIMPID      0xf13
#define CSR_MHARTID     0xf14

/* Trap setup */
#define CSR_MSTATUS     0x300
#define CSR_MISA        0x301
#define CSR_MEDELEG     0x302
#define CSR_MIDELEG     0x303
#define CSR_MIE         0x304
#define CSR_MTVEC       0x305
#define CSR_MCOUNTEREN  0x306

/* Trap handling */
#define CSR_MEPC        0x341
#define CSR_MCAUSE      0x342
#define CSR_MTVAL       0x343
#define CSR_MIP         0x344

/* Memory protection */
#define CSR_PMPCFG0     0x3a0
#define CSR_PMPADDR0    0x3b0
#define CSR_PMPADDR1    0x3b1
#define CSR_PMPADDR2    0x3b2
#define CSR_PMPADDR3    0x3b3

/* Counters */
#define CSR_MCYCLE      0xB00   /* Cycle counter */
#define CSR_MCYCLEH     0xB80   /* Upper 32-bits of cycle counter (RV32I) */

/*
 * CSR descriptions
 */

/* MSTATUS/SSTATUS registers */
#define STATUS_UIE         0x00000001   /* Interrupt Enable */
#define STATUS_SIE         0x00000002
#define STATUS_MIE         0x00000008

#define STATUS_UPIE        0x00000010   /* Previous Interrupt Enable */
#define STATUS_SPIE        0x00000020
#define STATUS_MPIE        0x00000080

#define STATUS_SPP         0x00000100   /* Previous Privilege mode */
#define STATUS_MPP         0x00001800
# define MPP_PRV_U         0x00000000
# define MPP_PRV_S         0x00000800
# define MPP_PRV_M         0x00001800

#define STATUS_FS          0x00006000   /* Floating-point status */
#define STATUS_XS          0x00018000   /* User-mode extensions status */
#define STATUS_MPRV        0x00020000   /* Modify PRiVilege */
#define STATUS_SUM         0x00040000   /* Supervisor User Memory access */
#define STATUS_MXR         0x00080000   /* Make eXecutable Readable */
#define STATUS_TVM         0x00100000   /* Trap Virtual Memory */
#define STATUS_TW          0x00200000   /* Timeout Wait */
#define STATUS_TSR         0x00400000   /* Trap SRET */
#define STATUS_SD32        0x80000000   /* Summary dirty status (FS/XS) */

/* MIP/SIP + MIE/SIE + MIDELEG/SIDELEG registers */
#define INT_USI     (1 << 0)    /* Software Interrupt */
#define INT_SSI     (1 << 1)
#define INT_MSI     (1 << 3)
#define INT_UTI     (1 << 4)    /* Timer Interrupt */
#define INT_STI     (1 << 5)
#define INT_MTI     (1 << 7)
#define INT_UEI     (1 << 8)    /* External Interrupt */
#define INT_SEI     (1 << 9)
#define INT_MEI     (1 << 11)

/* MCAUSE/SCAUSE registers */
#define CAUSE_FETCH_MISALIGNED      0
#define CAUSE_FETCH_FAULT           1
#define CAUSE_ILLEGAL_INSTRUCTION   2
#define CAUSE_BREAKPOINT            3
#define CAUSE_LOAD_MISALIGNED       4
#define CAUSE_LOAD_FAULT            5
#define CAUSE_STORE_MISALIGNED      6
#define CAUSE_STORE_FAULT           7
#define CAUSE_ECALL_USER            8
#define CAUSE_ECALL_SUPERVISOR      9
#define CAUSE_ECALL_HYPERVISOR      10
#define CAUSE_ECALL_MACHINE         11
#define CAUSE_PAGE_FAULT_FETCH      12
#define CAUSE_PAGE_FAULT_LOAD       13
#define CAUSE_PAGE_FAULT_STORE      15

/* PMP registers */
#define PMP_R   0x01        /* PMP configuration registers */
#define PMP_W   0x02
#define PMP_X   0x04
#define PMP_A   0x18
# define PMP_OFF    0x00
# define PMP_TOR    0x08
# define PMP_NA4    0x10
# define PMP_NAPOT  0x18
#define PMP_L   0x80
#define PMP_ADDR_SHIFT  2   /* PMP address registers (multiple of 4 bytes) */


/*
 * CSR access API
 */
#ifndef __ASSEMBLY__

/* Backend generic CSR accesses */
#define __CSR_READ(_op, _csr, _res)                     \
    __asm__ __volatile__("csr"#_op" %0, "#_csr          \
                         : "=r" (_res) :                \
                         : "memory"                     \
                         )
#define __CSR_WRITE(_op, _csr, _val)                    \
    __asm__ __volatile__("csr"#_op" "#_csr", %0"        \
                         :: "rJ" (_val)                 \
                         : "memory"                     \
                         )
#define __CSR_READ_WRITE(_op, _csr, _res, _val)         \
    __asm__ __volatile__("csr"#_op" %0, "#_csr", %1"    \
                         : "=r" (_res)                  \
                         : "rJ" (_val)                  \
                         : "memory"                     \
                         )

/* Frontend CSR accesses */
#define csr_read(csr)                       \
({                                          \
   unsigned long _res;                      \
   __CSR_READ(r, csr, _res);                \
   _res;                                    \
})

#define csr_read_clear(csr, val)            \
({                                          \
   unsigned long _res;                      \
   __CSR_READ_WRITE(rc, csr, _res, val);    \
   _res;                                    \
})

#define csr_read_write(csr, val)            \
({                                          \
   unsigned long _res;                      \
   __CSR_READ_WRITE(rw, csr, _res, val);    \
   _res;                                    \
})

#define csr_write(csr, val)                 __CSR_WRITE(w, csr, val)
#define csr_set(csr, val)                   __CSR_WRITE(s, csr, val)
#define csr_clear(csr, val)                 __CSR_WRITE(c, csr, val)


#define misa_extension(ext)                     \
({                                              \
    _Static_assert(ext >= 'A' && ext <= 'Z',    \
                   "Invalid MISA extension");   \
    csr_read(CSR_MISA) & (11 << (ext - 'A'));   \
})

#endif /* __ASSEMBLY__ */

#endif /* _CSR_H */

