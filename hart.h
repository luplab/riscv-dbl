// SPDX-License-Identifier: GPL-2.0
/*
 * Copyright (c) 2021 Joël Porquet-Lupine
 */
#ifndef _HART_H
#define _HART_H

#include <stdnoreturn.h>

void hart_stop(void);

noreturn void hart_setup(unsigned long hartid, void *dtb, void *entry);

#endif /* _HART_H */
