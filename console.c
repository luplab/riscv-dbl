// SPDX-License-Identifier: GPL-2.0
/*
 * Copyright (c) 2021 Joël Porquet-Lupine
 */
#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>

#include "console.h"
#include "fdt.h"
#include "string.h"

/*
 * Low-level console functions
 */
static console_putc_t console_driver;

static void console_putc(const char c)
{
    if (console_driver)
        console_driver(c);
}

static int console_puts(const char *s)
{
    int n = 0;

    while (*s) {
        if (*s == '\n')
            console_putc('\r');
        console_putc(*s);
        s++;
        n++;
    }

    return n;
}


/*
 * Frontend printing functions
 */
static int vsnprintf(char *buf, size_t size, const char *fmt, va_list ap)
{
    int n = 0;

    /* Error checking */
    if (!buf || (size <= 0))
        return -1;

    /* Helping macro: print the given character if we're still within the
     * buffer's bounds, otherwise only register the attempt */
#define __putchar(c)        \
    do {                    \
        if (n < (size - 1)) \
        *buf++ = (c);       \
        n++;                \
    } while(0)


    /* This first part deals with the format string: it scans it and
     * distinguishes between ordinary characters and conversion specifiers
     */
printf_text:

    /* Iterate through all the characters of the format string until the end
     * of the format string or the beginning of a conversion specifier ('%')
     */
    while (*fmt && *fmt != '%') {
        __putchar(*fmt++);
    }

    /* Found a conversion specifier */
    if (*fmt == '%') {
        fmt++;
        goto printf_arguments;
    }

    *buf = '\0';    /* terminating null byte */
    return n;       /* return what would have been entirely written */

    /* This second part processes conversion specifiers. It consumes an
     * argument and prints it as a string in accordance to the given
     * conversion specifier */
printf_arguments:
    {
        unsigned long long val = va_arg(ap, unsigned long); /* bigger size to accomodate -INT_MIN */
        char tmp[20]; /* Max number of decimal digits for a 64-bit unsigned */
        char *pbuf;
        static const char hexatab[] = "0123456789abcdef";
        size_t len = 0;

        /* Conversion specifier */
        switch (*fmt++) {

            case ('c'):     /* Char conversion */
                len = 1;
                tmp[0] = val;
                pbuf = tmp;
                break;

            case ('d'):     /* Decimal signed integer */
                if ((signed)val < 0) {
                    val = -(signed)val;
                    __putchar('-');
                }
            case ('u'):     /* Decimal unsigned integer */
                /* Convert decimal integer to string */
                do {
                    tmp[20 - ++len] = hexatab[val % 10];
                    val /= 10;
                } while (val);
                pbuf = &tmp[20 - len];
                break;

            case ('x'):     /* Hexadecimal integer */
                __putchar('0'); __putchar('x');
                /* Convert hexa integer to string */
                do {
                    tmp[20 - ++len] = hexatab[val % 16];
                    val /= 16;
                } while (val);
                pbuf = &tmp[20 - len];
                break;
            case ('s'):
                {
                    char *str = (char*)(uintptr_t)val;
                    len = strlen(str);
                    pbuf = str;
                    break;
                }
            default:
                goto printf_text;
        }

        /* Print the generated string */
        while (len--)
            __putchar(*pbuf++);
        goto printf_text;
    }

#undef __putchar
}

int printf(const char *fmt, ...)
{
    char buf[256];
    va_list ap;

    va_start(ap, fmt);
    vsnprintf(buf, sizeof(buf), fmt, ap);
    va_end(ap);

    return console_puts(buf);
}


/*
 * Backend console management
 */
void console_register(console_putc_t driver)
{
    console_driver = driver;
}

static int find_chosen_stdout(uintptr_t node, const char *name, int depth,
                              void *data)
{
    char **stdout_path = data;

    if (depth == 1 && !strcmp(name, "chosen")) {
        unsigned long l;
        char *p = fdt_get_prop(node, "stdout-path", &l);
        if (p && l) {
            *stdout_path = p;
            return 1;
        }
    }

    return 0;
}


static int find_console_driver(uintptr_t node, const char *name, int depth,
                               void *data)
{
    char *stdout_path = data;
    const char *p;
    const struct console_driver *d;

    extern const struct console_driver __console_driver_start[],
    __console_driver_end[];

    p = fdt_get_prop(node, "compatible", NULL);

    if (!p || (stdout_path && strstr(p, stdout_path)))
        return 0;

    for (d = __console_driver_start; d < __console_driver_end; d++) {
        if (!strcmp(d->compatible, p)) {
            return d->init(node);
        }
    }

    return 0;
}

void setup_console(void)
{
    char *stdout_path = NULL;

    /* Find chosen stdout device if any */
    fdt_scan(find_chosen_stdout, (void*)&stdout_path);

    /* Find matching console driver */
    fdt_scan(find_console_driver, stdout_path);
}
