// SPDX-License-Identifier: GPL-2.0
/*
 * Copyright (c) 2020 Joël Porquet-Lupine
 */
#include <stddef.h>
#include <stdint.h>

#include "dbl.h"
#include "fdt.h"
#include "string.h"

/*
 * Format definition
 */
#define FDT_MAGIC       0xd00dfeed
#define FDT_VERSION     17

#define FDT_BEGIN_NODE  1
#define FDT_END_NODE    2
#define FDT_PROP        3
#define FDT_NOP         4
#define FDT_END         9

struct fdt_header {
    uint32_t magic;
    uint32_t totalsize;
    uint32_t off_dt_struct;
    uint32_t off_dt_strings;
    uint32_t off_mem_rsvmap;
    uint32_t version;
    uint32_t last_comp_version; /* <= 17 */
    uint32_t boot_cpuid_phys;
    uint32_t size_dt_strings;
    uint32_t size_dt_struct;
};

static struct fdt_header *fdt;

static uint32_t fdt_addr_cells;
static uint32_t fdt_size_cells;

int fdt_init(void *dtb)
{
    if (!dtb)
        return -1;

    fdt = dtb;

    if (bswap32(fdt->magic) != FDT_MAGIC) {
        fdt = NULL;
        return -1;
    }

    return 0;
}

int fdt_find_root(uintptr_t node, const char *name, int depth, void *data)
{
    void *p;

    if (depth != 0)
        return 0;

    /* Default values */
    fdt_addr_cells = fdt_size_cells = 1;

    p = fdt_get_prop(node, "#address-cells", NULL);
    if (p)
        fdt_addr_cells = (uint32_t)bswap32(*(uint32_t*)p);

    p = fdt_get_prop(node, "#size-cells", NULL);
    if (p)
        fdt_size_cells = (uint32_t)bswap32(*(uint32_t*)p);

    return 1;
}

int fdt_scan(int (*cb)(uintptr_t node, const char *name, int depth, void *data),
             void *data)
{
    uintptr_t p = ((uintptr_t)fdt) + bswap32(fdt->off_dt_struct);
    int rc = 0;
    int depth = -1;

    while(1) {
        uint32_t token;
        const char *pathp;

        /* Read token */
        token = bswap32(*(uint32_t*)p);
        p += 4;

        /* Analyze token */
        if (token == FDT_END)
            /* Reached the end of the FDT */
            break;
        if (token == FDT_NOP)
            /* Skip */
            continue;
        if (token == FDT_END_NODE) {
            /* Come back from exploring a node */
            depth--;
            continue;
        }
        if (token == FDT_PROP) {
            /* Skip over entire property */
            uint32_t prop_sz = bswap32(*(uint32_t*)p);
            p += sizeof(uint32_t) + sizeof(uint32_t) + prop_sz;
            p = ALIGN(p, sizeof(uint32_t));
            continue;
        }
        if (token != FDT_BEGIN_NODE) {
            return -1;
        }

        depth++;

        /* Node's name/path */
        pathp = (char *)p;

        /* Start of next token */
        p = ALIGN(p + strlen(pathp) + 1, sizeof(uint32_t));

        /* Call callback function on node */
        rc = cb(p, pathp, depth, data);
        if (rc != 0)
            break;

    };

    return rc;
}

static const char *fdt_get_string(unsigned long offset)
{
    return (char*)fdt + bswap32(fdt->off_dt_strings) + offset;
}

void *fdt_get_prop(uintptr_t node, const char *name, unsigned long *size)
{
    uintptr_t p = node;

    /* Go through all the given node's properties */
    while (1) {
        uint32_t token;
        uint32_t prop_sz, prop_noff;
        const char *prop_nstr;

        /* Read token */
        token = bswap32(*(uint32_t*)p);
        p += 4;

        if (token == FDT_NOP)
            /* Skip */
            continue;
        if (token != FDT_PROP)
            /* No more properties to analyze */
            return NULL;

        /* Get property's size and offset in strings block */
        prop_sz = bswap32(*(uint32_t*)p);
        prop_noff = bswap32(*(uint32_t*)(p + 4));
        p += sizeof(uint32_t) + sizeof(uint32_t);

        /* Find property's name from strings block */
        prop_nstr = fdt_get_string(prop_noff);
        if (prop_nstr == NULL) {
            return NULL;
        }

        /* Compare to requested name */
        if (strcmp(name, prop_nstr) == 0) {
            if (size)
                *size = prop_sz;
            return (void *)p;
        }

        /* Skip to next token */
        p += prop_sz;
        p = ALIGN(p, sizeof(uint32_t));
    };
}

static inline uint64_t fdt_read_number(uint32_t **cellp, int size)
{
    uint32_t *cell = *cellp;
	uint64_t num = 0;

    *cellp = cell + size;

	for (; size--; cell++)
		num = (num << 32) | bswap32(*cell);

	return num;
}

void *fdt_read_addr_size(uint32_t *cellp, uint64_t *addr, uint64_t *len,
                         unsigned long size)
{
    uint64_t a, l;

    if (size % (sizeof(uint32_t) * (fdt_addr_cells + fdt_size_cells)) != 0)
        panic("Incorrect size for addr/size fields in FDT\n");

    a = fdt_read_number(&cellp, fdt_addr_cells);
    l = fdt_read_number(&cellp, fdt_size_cells);

    if (addr)
        *addr = a;
    if (len)
        *len = l;

    return cellp;
}
