// SPDX-License-Identifier: GPL-2.0
/*
 * Copyright (c) 2021 Joël Porquet-Lupine
 */
#include <stdint.h>
#include <stdnoreturn.h>

#include "csr.h"
#include "hart.h"

void hart_stop(void)
{
    while (1);
}

static void csr_init(void)
{
    /* Reset mstatus */
    csr_write(CSR_MSTATUS, 0);

    /* Enable all perf counters */
    csr_write(CSR_MCOUNTEREN, -1);

    /* Disable all individual interrupts */
    csr_write(CSR_MIE, 0);
}

static void fp_init(void)
{
    if (!misa_extension('F'))
        return;

    /* Dirty FP status by default */
    csr_set(CSR_MSTATUS, STATUS_FS);
    /* Reset FP control and status */
    csr_write(CSR_FCSR, 0);

#define reset_fp_reg(i) \
    asm volatile ("fmv.w.x f"#i", zero" : : : "f"#i);

    reset_fp_reg(0);  reset_fp_reg(1);  reset_fp_reg(2);  reset_fp_reg(3);
    reset_fp_reg(4);  reset_fp_reg(5);  reset_fp_reg(6);  reset_fp_reg(7);
    reset_fp_reg(8);  reset_fp_reg(9);  reset_fp_reg(10); reset_fp_reg(11);
    reset_fp_reg(12); reset_fp_reg(13); reset_fp_reg(14); reset_fp_reg(15);
    reset_fp_reg(16); reset_fp_reg(17); reset_fp_reg(18); reset_fp_reg(19);
    reset_fp_reg(20); reset_fp_reg(21); reset_fp_reg(22); reset_fp_reg(23);
    reset_fp_reg(24); reset_fp_reg(25); reset_fp_reg(26); reset_fp_reg(27);
    reset_fp_reg(28); reset_fp_reg(29); reset_fp_reg(30); reset_fp_reg(31);

#undef reset_fp_reg
}

static void deleg_traps(void)
{
    /* Delegate all S-mode interrupts directly */
    csr_write(CSR_MIDELEG, INT_SSI | INT_STI | INT_SEI);
    /* Delegate almost all S-mode exceptions directly */
    csr_write(CSR_MEDELEG,
              (1UL << CAUSE_FETCH_MISALIGNED)
              | (1UL << CAUSE_FETCH_FAULT)
              | (1UL << CAUSE_ILLEGAL_INSTRUCTION)
              | (1UL << CAUSE_BREAKPOINT)
              | (1UL << CAUSE_LOAD_MISALIGNED)
              | (1UL << CAUSE_LOAD_FAULT)
              | (1UL << CAUSE_STORE_MISALIGNED)
              | (1UL << CAUSE_STORE_FAULT)
              | (1UL << CAUSE_ECALL_USER)
              | (1UL << CAUSE_PAGE_FAULT_FETCH)
              | (1UL << CAUSE_PAGE_FAULT_LOAD)
              | (1UL << CAUSE_PAGE_FAULT_STORE));
}

static void setup_pmp(void)
{
    extern const uintptr_t __dbl_start[], __dbl_end[];
    volatile void *mtvec_copy = &&restore_mtvec;
    volatile int cond = 0;
    uintptr_t otvec;

    /* Ignore possible exception if some PMP registers are not implemented */
    otvec = csr_read_write(CSR_MTVEC, mtvec_copy);

    /* Ugly workaround to prevent GCC from moving label `restore_mtvec` */
    if (cond)
        goto *mtvec_copy;

    /* Reset configuration */
    csr_write(CSR_PMPCFG0, 0);

    /* Range for M-mode bootloader */
    csr_write(CSR_PMPADDR0, (uintptr_t)__dbl_start >> PMP_ADDR_SHIFT);
    csr_write(CSR_PMPADDR1, (uintptr_t)__dbl_end >> PMP_ADDR_SHIFT);
    csr_set(CSR_PMPCFG0, PMP_TOR << 8);

    /* Allow everything else for S-mode */
    csr_write(CSR_PMPADDR2, -1UL);
    csr_set(CSR_PMPCFG0, (PMP_NAPOT | PMP_X | PMP_W | PMP_R) << 16);

    /* MTVEC register containing label's address needs to be aligned on a 4-byte
     * boundary */
    asm volatile (".align 2");
restore_mtvec:
    csr_write(CSR_MTVEC, otvec);
}

noreturn
static void enter_supervisor(unsigned long hartid, void *dtb, void *entry)
{
    /*
     * Pretend like we're about to come back from a trap to machine mode.
     * MIE will be restored as disabled, and we will return from the trap in
     * supervisor mode at the kernel's entrypoint.
     */

    /* Set previous privilege mode as supervisor */
    csr_clear(CSR_MSTATUS, STATUS_MPP);
    csr_set(CSR_MSTATUS, MPP_PRV_S);

    /* Resume at kernel's entrypoint */
    csr_write(CSR_MEPC, entry);

    /*
     * Reset some S-mode CSR registers
     *
     * SSTATUS doesn't need to be reset, as it's only a subset of MSTATUS which
     * we have already configured properly.
     */
    /* Make S-mode trap vector also be the kernel's entrypoint */
    csr_write(CSR_STVEC, entry);
    csr_write(CSR_SSCRATCH, 0);
    csr_write(CSR_SIE, 0);
    csr_write(CSR_SATP, 0);

    /*
     * Place @hartid and @dtb in a0 and a1 and return from the trap
     */
    register unsigned long a0 asm ("a0") = hartid;
    register unsigned long a1 asm ("a1") = (unsigned long)dtb;
    asm volatile ("mret" : : "r" (a0), "r" (a1) : "memory" );

    /* We should never come back here */
    __builtin_unreachable();
}

noreturn
void hart_setup(unsigned long hartid, void *dtb, void *entry)
{
    csr_init();
    fp_init();
    deleg_traps();
    setup_pmp();
    enter_supervisor(hartid, dtb, entry);
}

