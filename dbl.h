// SPDX-License-Identifier: GPL-2.0
/*
 * Copyright (c) 2021 Joël Porquet-Lupine
 */
#ifndef _DBL_H
#define _DBL_H

#include "console.h"
#include "hart.h"

/*
 * Alignment
 */
#define __ALIGN_MASK(x, mask)   (((x) + (mask)) & ~(mask))
#define ALIGN(x, a)             __ALIGN_MASK(x, ((typeof(x))(a) - 1))

/*
 * Unused parameter
 */
#define __unused  __attribute__((unused))

/*
 * Panicking
 */
#define panic(fmt, ...)                         \
do {                                            \
    printf("DBL panic: " fmt, ##__VA_ARGS__);   \
    hart_stop();                                \
} while(0)

#endif /* _DBL_H */

