// SPDX-License-Identifier: GPL-2.0
/*
 * Copyright (c) 2021 Joël Porquet-Lupine
 */
#include <stdatomic.h>
#include <stdint.h>
#include <stdnoreturn.h>

#include "console.h"
#include "csr.h"
#include "dbl.h"
#include "fdt.h"
#include "hart.h"
#include "string.h"

static void zero_bss(void)
{
    extern unsigned long __bss_start[], __bss_end[];
    unsigned long *b;

    for (b = __bss_start; b < __bss_end; b++)
        *b = 0;
}

static void *kernel_entry;

static int find_chosen_kernel(uintptr_t node, const char *name, int depth,
                              void *data __unused)
{
    if (depth == 1 && !strcmp(name, "chosen")) {
        unsigned long s;
        void *p = fdt_get_prop(node, "kernel", &s);
        if (!p)
            return -1;

        fdt_read_addr_size(p, (uint64_t*)&kernel_entry, NULL, s);

        return 1;
    }

    return 0;
}

static void dtb_parse(void *dtb)
{
    if (fdt_init(dtb))
        hart_stop();
    fdt_scan(fdt_find_root, NULL);

    /* Setup console */
    setup_console();
    printf("DBL - the dumb bootloader\n");

    /* Find kernel address */
    if (fdt_scan(find_chosen_kernel, NULL) != 1)
        panic("Unable to find `/chosen/kernel` property in DTB\n");
}

static void primary_setup(void *dtb)
{
    zero_bss();
    dtb_parse(dtb);

    if (!misa_extension('S'))
        panic("No S-mode available\n");
}

noreturn void dbl_main(unsigned long hartid, void *dtb)
{
    /* Non-zero initialization is important for this global barrier, as it makes
     * it be immediately functional without relying on bss section to be reset
     * first */
    static unsigned long secondary_wait = ATOMIC_VAR_INIT(1);

    if (hartid == 0) {
        /* General setup performed by primary hart */
        primary_setup(dtb);

        /* Unlock all the other harts */
        atomic_store(&secondary_wait, 0);
    } else {
        /* Wait for primary hart to give its go-ahead */
        while (atomic_load(&secondary_wait));
    }

    /* Configure hart and jump into kernel */
    hart_setup(hartid, dtb, kernel_entry);
}

void dbl_trap(unsigned long regs[32])
{
    unsigned long mcause, mepc, mstatus, mtval, mhartid;

    /* Get more info from CSRs */
    mcause = csr_read(CSR_MCAUSE);
    mepc = csr_read(CSR_MEPC);
    mstatus = csr_read(CSR_MSTATUS);
    mtval = csr_read(CSR_MTVAL);
    mhartid = csr_read(CSR_MHARTID);

    /* Reject all ecalls from S-mode */
    if (mcause == CAUSE_ECALL_SUPERVISOR) {
        printf("DBL: reject S-mode ecall\n");
        /* Skip over ecall instruction */
        csr_write(CSR_MEPC, mepc + 4);
        /* Return error in a0 */
        regs[10] = -1;

        return;
    }

    /* Unhandled interrupts or exceptions */
    printf("mcause=%x\nmtval=%x\nmstatus=%x\nmepc=%x\n",
           mcause,   mtval,    mstatus,  mepc);
    printf("x0/zero=%x\nx1/ra=%x\nx2/sp=%x\nx3/gp=%x\n",
           regs[0],  regs[1],  regs[2],  regs[3]);
    printf("x4/tp=%x\nx5/t0=%x\nx6/t1=%x\nx7/t2=%x\n",
           regs[4],  regs[5],  regs[6],  regs[7]);
    printf("x8/s0=%x\nx9/s1=%x\nx10/a0=%x\nx11/a1=%x\n",
           regs[8],  regs[9],  regs[10], regs[11]);
    printf("x12/a2=%x\nx13/a3=%x\nx14/a4=%x\nx15/a5=%x\n",
           regs[12], regs[13], regs[14], regs[15]);
    printf("x16/a6=%x\nx17/a7=%x\nx18/s2=%x\nx19/s3=%x\n",
           regs[16], regs[17], regs[18], regs[19]);
    printf("x20/s4=%x\nx21/s5=%x\nx22/s6=%x\nx23/s7=%x\n",
           regs[20], regs[21], regs[22], regs[23]);
    printf("x24/s8=%x\nx25/s9=%x\nx26/s10=%x\nx27/s11=%x\n",
           regs[24], regs[25], regs[26], regs[27]);
    printf("x28/t3=%x\nx29/t4=%x\nx30/t5=%x\nx31/t6=%x\n",
           regs[28], regs[29], regs[30], regs[31]);
    panic("DBL unhandled trap on hart %d\n", mhartid);
}
