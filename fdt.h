// SPDX-License-Identifier: GPL-2.0
/*
 * Copyright (c) 2021 Joël Porquet-Lupine
 */
#ifndef _FDT_H
#define _FDT_H

#include <stdint.h>

int fdt_init(void *dtb);

int fdt_find_root(uintptr_t node, const char *name, int depth, void *data);

int fdt_scan(int (*cb)(uintptr_t node, const char *name, int depth, void *data),
             void *data);

void *fdt_get_prop(uintptr_t node, const char *name, unsigned long *size);

void *fdt_read_addr_size(uint32_t *cellp, uint64_t *addr, uint64_t *len,
                         unsigned long size);

/* FDT uses big-endian, while RISC-V is little-endian */
static inline uint32_t bswap32(uint32_t x)
{
    return (((x & (uint32_t)0x000000FFUL) << 24) | \
            ((x & (uint32_t)0x0000FF00UL) <<  8) | \
            ((x & (uint32_t)0x00FF0000UL) >>  8) | \
            ((x & (uint32_t)0xFF000000UL) >> 24));
}
static inline uint64_t bswap64(uint64_t x)
{
    return (((x & (uint64_t)0x00000000000000FFULL) << 56) |	\
            ((x & (uint64_t)0x000000000000FF00ULL) << 40) |	\
            ((x & (uint64_t)0x0000000000FF0000ULL) << 24) |	\
            ((x & (uint64_t)0x00000000FF000000ULL) <<  8) |	\
            ((x & (uint64_t)0x000000FF00000000ULL) >>  8) |	\
            ((x & (uint64_t)0x0000FF0000000000ULL) >> 24) |	\
            ((x & (uint64_t)0x00FF000000000000ULL) >> 40) |	\
            ((x & (uint64_t)0xFF00000000000000ULL) >> 56));
}
#define bswap(x) _Generic((x), uint32_t: bswap32, uint64_t: bswap64)(x)

#endif /* _FDT_H */
