// SPDX-License-Identifier: GPL-2.0
/*
 * Copyright (c) 2021 Joël Porquet-Lupine
 */
#include <stdint.h>

#include "string.h"

size_t strlen(const char *s)
{
    const char *sc;

    for (sc = s; *sc != '\0'; ++sc)
        /* nothing */;

    return sc - s;
}

size_t strnlen(const char *s, size_t maxlen)
{
    const char *sc;

    for (sc = s; maxlen-- && *sc != '\0'; ++sc)
        /* nothing */;

    return sc - s;
}

int strcmp(const char *s1, const char *s2)
{
    signed char r;

    while (1) {
        if ((r = *s1 - *s2++) != 0 || !*s1++)
            break;
    }

    return r;
}

char *strstr(const char *s1, const char *s2)
{
	size_t l1, l2;

	l2 = strlen(s2);
	if (!l2)
		return (char*)s1;
	l1 = strlen(s1);
	while (l1 >= l2) {
		l1--;
		if (!memcmp(s1, s2, l2))
			return (char*)s1;
		s1++;
	}

	return NULL;
}

void *memset(void *s, int c, size_t n)
{
    char *sb = s;

    while (n--)
        *sb++ = c;

    return s;
}

int memcmp(const void *s1, const void *s2, size_t n)
{
	const unsigned char *a1, *a2;
	int res = 0;

	for (a1 = s1, a2 = s2; n > 0; a1++, a2++, n--)
		if ((res = *a1 - *a2) != 0)
			break;

	return res;
}

