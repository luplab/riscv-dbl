# The Dumb Bootloader

## Motivation

Existing bootloaders for RISC-V systems (e.g.
[BBL](https://github.com/riscv-software-src/riscv-pk) or
[OpenSBI](https://github.com/riscv-software-src/opensbi)) are too complex and
opinionated. They require the platform to be equipped with specific hardware
devices, such as the CLINT or the PLIC, and provide an extensive API to S-mode
software via their SBI interface.

For basic scenarios, like running the Linux kernel on a simple QEMU platform,
all of this complexity is unnecessary. Linux already knows how to implement the
services provided by SBI (e.g., setting up timers, delivering IPIs, etc.).
Besides, RISC-V based hardware should ideally not be dependent on certain
physical devices.

DBL is idiotically simple, hence the name. DBL only prepares the harts and jumps
in S-mode to where the kernel is located. All S-mode events are configured to be
delegated directly to S-mode software. If no devices are connected to the M-mode
processor pins, then DBL should never be called again after the kernel has
started running.

DBL makes no assumption about the hardware, and only relies on a couple of
fields found in the FDT loaded by the previous stage.

## Acronyms

Depending on your mood, DBL can stand for multiple things:

- The *Dumb Bootloader* (officially)
- *Débile* (means dumb in French, which is exactly what this bootloader is!)
- *Davis Bootloader* (because Davis and Berkeley are rival siblings)

## Credit and license

`dbl` is written and supported by Joël Porquet-Lupine at the
[LupLab](https://luplab.cs.ucdavis.edu/) and is licensed under the
[GPLv2](http://www.gnu.org/licenses/gpl-2.0.html)+.

The project's logo is derived from the [Twemoji](https://twemoji.twitter.com/)
emoji library / [CC-BY](https://creativecommons.org/licenses/by/4.0/).
