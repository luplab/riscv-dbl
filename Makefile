# SPDX-License-Identifier: GPL-2.0
#
# Copyright (c) 2021 Joël Porquet-Lupine

# Avoid builtin rules and variables
MAKEFLAGS += -rR

# Build directory
ifeq ($(O),)
BUILD := $(CURDIR)/build
else
BUILD := $(shell readlink -f $(O))
endif

# Default rule: build the kernel image
all: $(BUILD) $(BUILD)/dbl $(BUILD)/dbl.bin


# User configuration
DEF_CONF := config_lupv64.mk
CONF ?= $(DEF_CONF)

## Specific configuration from user
include $(CONF)

## Check that all known configuration variables are defined
CONF_VARS=CONFIG_XLEN CONFIG_LOAD_ADDR CONFIG_MAX_HARTS CONFIG_STACK_SIZE_LOG2
$(foreach v,$(CONF_VARS),$(if $($(v)),,$(error Error: $(v) is undefined)))

## 64bit vs 32bit
ifeq ($(CONFIG_XLEN),64)
ABI=lp64
else ifeq ($(CONFIG_XLEN),32)
ABI=ilp32
else
$(error Invalid or missing XLEN (=$(CONFIG_XLEN)))
endif


# Compilation tools

## Define compilation toolchain
CC		= $(CROSS_COMPILE)gcc
CPP		= $(CROSS_COMPILE)cpp
LD		= $(CROSS_COMPILE)gcc
AS		= $(CROSS_COMPILE)as
OBJCOPY	= $(CROSS_COMPILE)objcopy

## Try to find a suitable cross-compiler
ifeq ($(CROSS_COMPILE),)
cc-cross-prefix = $(word 1, $(foreach c,$(1),				\
			$(shell set -e;									\
			if (which $(strip $(c))$(CC)) > /dev/null 2>&1;	\
			then											\
				echo $(c);									\
			fi)))
CROSS_COMPILE := $(call cc-cross-prefix, riscv64-unknown-linux-gnu- \
                                         riscv64-linux-gnu-)
endif

# Compilation flags
## General GCC options
CFLAGS := -std=gnu11        # Compatible with C11+GNU extensions
CFLAGS += -Wall				# Always show warnings
CFLAGS += -Werror			# Warnings are errors
CFLAGS += -pipe				# Use pipes between compilation stages
CFLAGS += -ffreestanding	# Freestanding environment (no builtin)
CFLAGS += -MMD				# Generate dependencies
## Debugging
ifeq ($(D),1)
CFLAGS += -g -gdwarf-2		# Debugging information
else
CFLAGS += -O2				# Optimization level
endif

## RISC-V-specific machine flags
mflags := -march=rv$(CONFIG_XLEN)gc	# Typical rv64gc or rv32gc
mflags += -mabi=$(ABI)				# Softfloat ABI

### RISC-V-specific GCC options
CFLAGS += $(mflags)
CFLAGS += -mcmodel=medany			# Symbols within any 2GiB range
CFLAGS += -fno-pic					# Disable PIC code generation
CFLAGS += -mno-save-restore			# Don't use smaller function prologues
CFLAGS += -mstrict-align			# Don't emit unaligned memory accesses

### User config
CFLAGS += $(foreach v,$(CONF_VARS),-D$(v)=$($(v)))

## Assembler options
AFLAGS := -D__ASSEMBLY__
AFLAGS += $(CFLAGS)

## Linker options
LDFLAGS := $(mflags)
LDFLAGS += -nostdlib				# Do not include standard libraries
LDFLAGS += -static					# Static executable
LDFLAGS += -Wl,-build-id=none		# No build ID

## Linker libs
LDLIBS := $(shell $(CC) $(CFLAGS) -print-libgcc-file-name) # GCC library

## Include paths
INCLUDE := -nostdinc		# Do not use standard library headers
INCLUDE += -isystem $(shell $(CC) $(CFLAGS) -print-file-name=include) # Use GCC's builtin headers

## Strip out the flags and options before using them in the rules below
_cflags  := $(strip  $(CFLAGS))
_aflags  := $(strip  $(AFLAGS))
_ldflags := $(strip $(LDFLAGS))
_ldlibs  := $(strip $(LDLIBS))
_include := $(strip $(INCLUDE))


# Compilation files

## Objects
head := entry.o
objs := \
    dbl.o		\
    console.o	\
    fdt.o		\
    hart.o		\
    lupio_tty.o \
    string.o
head := $(addprefix $(BUILD)/,$(head))
objs := $(addprefix $(BUILD)/,$(objs))

## Preprocessed linker script
lds := dbl.lds
lds := $(addprefix $(BUILD)/,$(lds))


# Dependency mangement
deps := $(patsubst %.o,%.d,$(objs))
deps += $(patsubst %.lds,%.d,$(lds))
-include $(deps)


# Compilation rules

## Don't print the commands unless explicitely requested with `make V=1`
ifneq ($(V),1)
Q = @
endif

$(BUILD):
	$(Q)mkdir -p $@

## Main rules for building the kernel images
$(BUILD)/dbl.bin: $(BUILD)/dbl
	@echo "OBJCPY	$@"
	$(Q)$(OBJCOPY) -O binary --remove-section=.note --remove-section=.comment $< $@

$(BUILD)/dbl: $(head) $(objs) $(lds)
	@echo "LD	$@"
	$(Q)$(LD) $(_ldflags) -o $@ -T $(lds) $(head) $(objs) $(_ldlibs)

## Generic rule for compiling assembly files
$(BUILD)/%.o: %.S
	@echo "AS	$@"
	$(Q)$(CC) $(_aflags) $(_include) -c -o $@ $<

## Generic rule for compiling C files
$(BUILD)/%.o: %.c
	@echo "CC	$@"
	$(Q)$(CC) $(_cflags) $(_include) -c -o $@ $<

## Generic rule for preprocessing lds.S files
$(BUILD)/%.lds: %.lds.S
	@echo "LDS	$@"
	$(Q)$(CPP) $(_aflags) $(_include) -MQ $@ -P -o $@ $<

## Rule for cleaning
clean:
	@echo "CLEAN	."
	$(Q)rm -rf $(BUILD)

## Help rule
help:
	@echo "  make V=1           Verbose build"
	@echo "  make O=<dir>       Build directory"
	@echo "  make CONF=<file>   Configuration file"
	@echo "                     (default is '$(DEF_CONF)')"

