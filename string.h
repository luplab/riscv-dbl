// SPDX-License-Identifier: GPL-2.0
/*
 * Copyright (c) 2021 Joël Porquet-Lupine
 */
#ifndef _STRING_H
#define _STRING_H

#include <stddef.h>

/*
 * String functions
 */
size_t strlen(const char *s);
size_t strnlen(const char *s, size_t maxlen);

int strcmp(const char *s1, const char *s2);
char *strstr(const char *s1, const char *s2);

/*
 * Memory functions
 */
void *memset(void *s, int c, size_t n);
int memcmp(const void *s1, const void *s2, size_t n);

#endif /* _STRING_H */
